import unittest

import grpc
import echo_pb2

from servicer import Servicer

class ServicerContext(grpc.ServicerContext):
    def __init__(self):
        self.code = None
        self.details = None

    def add_callback(self):
        pass

    def cancel(self):
        pass

    def is_active(self):
        pass

    def time_remaining(self):
        pass

    def abort(self, code, details):
        pass

    def auth_context(self):
        pass

    def invocation_metadata(self):
        pass

    def peer(self):
        pass

    def peer_identities(self):
        pass

    def peer_identity_key(self):
        pass

    def send_initial_metadata(self, initial_metadata):
        pass

    def set_code(self, code):
        self.code = code

    def set_details(self, details):
        self.details = details

    def set_trailing_metadata(self, trailing_metadata):
        pass


class TestServicer(unittest.TestCase):
    def test_Servicer(self):
        servicer = Servicer()
        request  = echo_pb2.Request(message="Test Message")
        context  = ServicerContext()

        response = servicer.Echo(request, context)
        self.assertEqual(echo_pb2.Response(message="Test Message"), response)

    def test_Servicer_error(self):
        servicer = Servicer()
        request  = echo_pb2.Request(message="unko")
        context  = ServicerContext()

        with self.assertRaises(Exception):
            servicer.Echo(request, context)

        self.assertEqual(grpc.StatusCode.OUT_OF_RANGE, context.code)
        self.assertEqual("unko is a prohibited word.", context.details)


if __name__ == "__main__":
    unittest.main()
