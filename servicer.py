import grpc
import echo_pb2
import echo_pb2_grpc

_BANNED_WORDS = ["unko"]

class Servicer(echo_pb2_grpc.EchoServicer):
    def __init__(self):
        pass

    def Echo(self, request, context):
        if request.message in _BANNED_WORDS:
            context.set_code(grpc.StatusCode.OUT_OF_RANGE)
            context.set_details(f"{request.message} is a prohibited word.")
            raise Exception(f"'{request.message}' is a prohibited word.")
        return echo_pb2.Response(message=request.message)
