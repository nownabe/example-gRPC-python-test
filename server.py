from concurrent import futures
import time

import grpc
import echo_pb2_grpc

from servicer import Servicer

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

def serve():
    servicer = Servicer()
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    echo_pb2_grpc.add_EchoServicer_to_server(servicer, server)
    server.add_insecure_port("[::]:50051")
    server.start()

    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == "__main__":
    serve()
