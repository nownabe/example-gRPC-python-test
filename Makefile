echo_pb2.py: echo.proto
	python -m grpc_tools.protoc \
		-I=. \
		--python_out=. \
		--grpc_python_out=. \
		$?

test:
	python -m unittest tests/test_*.py
