import grpc

import grpc
import echo_pb2
import echo_pb2_grpc

def run():
    channel = grpc.insecure_channel("[::]:50051")
    stub = echo_pb2_grpc.EchoStub(channel)

    message = "Hello, world!"

    response = stub.Echo(echo_pb2.Request(message=message))
    print(f"Response: {response.message}")

if __name__ == "__main__":
    run()
